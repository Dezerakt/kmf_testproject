package configs

import (
	"fmt"
	"github.com/ilyakaznacheev/cleanenv"
	"kmf_test/internal/models"
)

type (
	Config struct {
		App    `yaml:"app"`
		HTTP   `yaml:"http"`
		Log    `yaml:"logger"`
		PG     `yaml:"postgres"`
		WebAPI `yaml:"webapi"`
	}

	App struct {
		Name    string `yaml:"name"    env:"APP_NAME"`
		Version string `yaml:"version" env:"APP_VERSION"`
	}

	// HTTP -.
	HTTP struct {
		Port string `yaml:"port" env:"SERVER_PORT"`
	}

	// Log -.
	Log struct {
		Level string `yaml:"log_level"   env:"LOG_LEVEL"`
	}

	// PG -.
	PG struct {
		Host     string `env-required:"true" yaml:"host" env:"DB_HOST"`
		User     string `env-required:"true" yaml:"user" env:"DB_USER"`
		Password string `env-required:"true" yaml:"password" env:"DB_PASSWORD"`
		Name     string `env-required:"true" yaml:"name" env:"DB_NAME"`
		Port     string `env-required:"true" yaml:"port" env:"DB_PORT"`
	}

	WebAPI struct {
		NationalBankURL string `env-required:"true" yaml:"national_bank"`
	}
)

var (
	TablesList = []interface{}{
		models.Currency{},
	}
)

// NewConfig returns app config.
func NewConfig() (*Config, error) {
	cfg := &Config{}

	err := cleanenv.ReadConfig("./config.yml", cfg)
	if err != nil {
		return nil, fmt.Errorf("config error: %w", err)
	}

	err = cleanenv.ReadEnv(cfg)
	if err != nil {
		return nil, err
	}

	return cfg, nil
}
