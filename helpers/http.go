package helpers

import (
	"bytes"
	"errors"
	"net/http"
	"strconv"
	"time"
)

func JsonPostRequest(url string, body []byte) (string, error) {
	request, err := CreateNewRequest(
		"POST",
		url,
		bytes.NewBuffer(body),
		map[string]string{
			"Content-Type": "application/json",
		})
	if err != nil {
		return "", err
	}

	response, err := DoRequest(request)
	if err != nil {
		return "", err
	}

	responseBody, err := ReadResponseBody(response)
	if err != nil {
		return "", err
	}

	return responseBody, nil
}

func CreateNewRequest(method string, url string, content *bytes.Buffer, headers map[string]string) (*http.Request, error) {
	if content == nil {
		content = bytes.NewBuffer([]byte{})
	}

	req, err := http.NewRequest(
		method,
		url,
		content,
	)
	if err != nil {
		return nil, err
	}

	for key, value := range headers {
		req.Header.Set(key, value)
	}

	return req, nil
}

func DoRequest(request *http.Request) (*http.Response, error) {
	client := &http.Client{Timeout: time.Second * 30}

	resp, err := client.Do(request)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK && resp.StatusCode != 201 {
		return nil, errors.New("status code: " + strconv.Itoa(resp.StatusCode))
	}

	return resp, nil
}

func ReadResponseBody(httpResponse *http.Response) (string, error) {
	defer httpResponse.Body.Close()

	buf := new(bytes.Buffer)
	_, err := buf.ReadFrom(httpResponse.Body)
	if err != nil {
		return "", err
	}

	return buf.String(), nil
}
