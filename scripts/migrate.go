package main

import (
	"kmf_test/configs"
	"kmf_test/pkg/gorm"
	"log"
)

func main() {
	cfg, err := configs.NewConfig()
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Migration Start")

	gormObject, err := gorm.New(cfg)
	if err != nil {
		return
	}

	err = gormObject.AutoMigrate(configs.TablesList...)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Migration end")
}
