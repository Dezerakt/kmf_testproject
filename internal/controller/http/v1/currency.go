package v1

import (
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"kmf_test/internal/controller/http/v1/responses"
	"kmf_test/internal/usecase"
	"kmf_test/pkg/logger"
)

type currencyRoutes struct {
	u usecase.Currency
	l logger.Interface
}

// newCurrencyRoutes -.
func newCurrencyRoutes(handler *gin.RouterGroup, logger logger.Interface, currencyUseCase usecase.Currency) {
	r := &currencyRoutes{
		currencyUseCase,
		logger}

	h := handler.Group("/currency")
	{
		h.GET("/save/:date", r.saveCurrency)
		h.GET("/:date", r.getCurrency)
		h.GET("/:date/:code", r.getCurrency)
	}
}

// saveCurrency -.
func (obj *currencyRoutes) saveCurrency(ctx *gin.Context) {
	date := ctx.Param("date")
	if date == "" {
		responses.Error(ctx, errors.New("date param not found"))
	}

	err := obj.u.SaveCurrency(date)
	if err != nil {
		responses.Error(ctx, err)
		return
	}

	ctx.JSON(200, gin.H{
		"success": true,
	})
}

// getCurrency -.
func (obj *currencyRoutes) getCurrency(ctx *gin.Context) {
	date := ctx.Param("date")
	if date == "" {
		responses.Error(ctx, errors.New("date param not found"))
	}

	result, err := obj.u.GetCurrency(ctx.Param("date"), ctx.Param("code"))
	if err != nil {
		responses.Error(ctx, err)
		return
	}

	responses.Done(ctx, result)
}
