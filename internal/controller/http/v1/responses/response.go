package responses

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func Error(ctx *gin.Context, err error) {
	ctx.JSON(500, gin.H{
		"error": err.Error(),
	})
}

func Done(ctx *gin.Context, result interface{}) {
	if result == nil {
		result = "successfully"
	}
	ctx.Writer.Header().Set("Transfer-Encoding", "chunked")

	ctx.JSON(http.StatusOK, gin.H{
		"data": result,
	})
}
