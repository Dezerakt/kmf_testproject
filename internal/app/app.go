package app

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"kmf_test/configs"
	v1 "kmf_test/internal/controller/http/v1"
	"kmf_test/internal/usecase"
	"kmf_test/internal/usecase/repo"
	"kmf_test/internal/usecase/webapi"
	"kmf_test/pkg/gorm"
	"kmf_test/pkg/httpserver"
	"kmf_test/pkg/logger"
	"os"
	"os/signal"
	"syscall"
)

func Run(cfg *configs.Config) {
	l := logger.New(cfg.Log.Level)

	// Repository
	gormObject, err := gorm.New(cfg)
	if err != nil {
		l.Fatal(fmt.Errorf("app - Run - gorm.NewUserRepo: %w", err))
	}

	// Use case
	currencyUseCase := usecase.NewCurrencyUseCase(
		repo.NewCurrencyRepo(gormObject),
		webapi.NewCurrencyNationalBank(cfg.WebAPI.NationalBankURL),
	)

	handler := gin.New()

	// HTTP Server
	v1.NewRouter(handler, l, currencyUseCase)
	httpServer := httpserver.New(handler, httpserver.Port(cfg.HTTP.Port))

	// Waiting signal
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)

	select {
	case s := <-interrupt:
		l.Info("app - Run - signal: " + s.String())
	case err = <-httpServer.Notify():
		l.Error(fmt.Errorf("app - Run - httpServer.Notify: %w", err))
	}

	// Shutdown
	err = httpServer.Shutdown()
	if err != nil {
		l.Error(fmt.Errorf("app - Run - httpServer.Shutdown: %w", err))
	}
}
