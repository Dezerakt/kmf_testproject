package models

import "encoding/xml"

type Rates struct {
	XMLName xml.Name       `xml:"rates"`
	Date    string         `xml:"date"`
	Items   []CurrencyItem `xml:"item"`
}

type CurrencyItem struct {
	XMLName     xml.Name `xml:"item"`
	Fullname    string   `xml:"fullname"`
	Title       string   `xml:"title"`
	Description string   `xml:"description"`
	Quant       string   `xml:"quant"`
	Index       string   `xml:"index"`
	Change      string   `xml:"change"`
}
