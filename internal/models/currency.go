package models

import (
	"gorm.io/gorm"
	"time"
)

type Currency struct {
	gorm.Model
	Title string    `gorm:"column:TITLE; size:60; not null; comment:currency name"`
	Code  string    `gorm:"column:CODE; size:3; not null; comment:currency code"`
	Value float64   `gorm:"column:VALUE; type:numeric(18,2); not null; comment:value"`
	ADate time.Time `gorm:"column:A_DATE; type:timestamp without time zone; not null; comment:date of unloading"`
}

func (Currency) TableName() string {
	return "R_CURRENCY"
}
