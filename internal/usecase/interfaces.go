package usecase

import "kmf_test/internal/models"

type (
	Currency interface {
		SaveCurrency(string) error
		GetCurrency(string, string) (interface{}, error)
	}

	CurrencyRepo interface {
		SaveCurrency(currency *models.Currency) error
		GetCurrency(currency *models.Currency) ([]models.Currency, error)
	}

	CurrencyWebApi interface {
		GetCurrency(date string) (interface{}, error)
	}
)
