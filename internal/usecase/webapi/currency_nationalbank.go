package webapi

import (
	"bytes"
	"fmt"
	"net/http"
)

type CurrencyNationalBank struct {
	nationalBankApi string
}

func NewCurrencyNationalBank(url string) *CurrencyNationalBank {
	return &CurrencyNationalBank{
		nationalBankApi: url,
	}
}

func (obj *CurrencyNationalBank) GetCurrency(date string) (interface{}, error) {
	response, err := http.Get(fmt.Sprintf(obj.nationalBankApi+"/rss/get_rates.cfm?fdate=%s", date))
	if err != nil {
		return nil, err
	}

	defer response.Body.Close()

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(response.Body)
	if err != nil {
		return "", err
	}

	return buf.String(), nil
}
