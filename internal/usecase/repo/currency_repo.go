package repo

import (
	"gorm.io/gorm"
	"kmf_test/internal/models"
)

type CurrencyRepo struct {
	gormObject *gorm.DB
}

func NewCurrencyRepo(gormObject *gorm.DB) *CurrencyRepo {
	return &CurrencyRepo{gormObject: gormObject}
}

func (obj *CurrencyRepo) SaveCurrency(newCurrencyRecord *models.Currency) error {
	err := obj.gormObject.Create(newCurrencyRecord).Error
	if err != nil {
		return err
	}
	return nil
}

func (obj *CurrencyRepo) GetCurrency(newCurrencyRecord *models.Currency) ([]models.Currency, error) {
	var result []models.Currency

	err := obj.gormObject.Find(&result, newCurrencyRecord).Error
	if err != nil {
		return nil, err
	}
	return result, nil
}
