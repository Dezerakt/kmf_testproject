package usecase

import (
	"encoding/xml"
	"kmf_test/internal/models"
	"log"
	"strconv"
	"time"
)

type CurrencyUseCase struct {
	CurrencyRepo
	CurrencyWebApi
}

func NewCurrencyUseCase(repo CurrencyRepo, webApi CurrencyWebApi) *CurrencyUseCase {
	return &CurrencyUseCase{
		CurrencyRepo:   repo,
		CurrencyWebApi: webApi,
	}
}

func (obj CurrencyUseCase) SaveCurrency(date string) error {
	rawXMLData, err := obj.CurrencyWebApi.GetCurrency(date)
	if err != nil {
		return err
	}

	var rates models.Rates

	err = xml.Unmarshal([]byte(rawXMLData.(string)), &rates)
	if err != nil {
		return err
	}

	for _, currency := range rates.Items {
		go func(arrivedCurrency models.CurrencyItem) {
			parsedTime, err := time.Parse("02.01.2006", rates.Date)
			if err != nil {
				log.Println(err.Error())
				return
			}

			descFloat, err := strconv.ParseFloat(arrivedCurrency.Description, 64)
			if err != nil {
				log.Println(err.Error())
				return
			}

			err = obj.CurrencyRepo.SaveCurrency(&models.Currency{
				Title: arrivedCurrency.Fullname,
				Code:  arrivedCurrency.Title,
				Value: descFloat,
				ADate: parsedTime,
			})
			if err != nil {
				log.Println(err.Error())
				return
			}
		}(currency)
	}

	return nil
}

func (obj CurrencyUseCase) GetCurrency(rawTime string, code string) (interface{}, error) {
	parsedTime, err := time.Parse("02.01.2006", rawTime)
	if err != nil {
		return nil, err
	}

	currenciesList, err := obj.CurrencyRepo.GetCurrency(&models.Currency{
		ADate: parsedTime,
		Code:  code,
	})
	if err != nil {
		return nil, err
	}

	return currenciesList, nil
}
