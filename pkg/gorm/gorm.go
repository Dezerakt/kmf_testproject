package gorm

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"kmf_test/configs"
)

func New(config *configs.Config) (*gorm.DB, error) {
	connectionString := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable",
		config.PG.Host,
		config.PG.User,
		config.PG.Password,
		config.PG.Name,
		config.PG.Port)
	gormObject, err := gorm.Open(postgres.Open(connectionString), &gorm.Config{})
	if err != nil {
		return nil, err
	}

	return gormObject, nil
}
